var fs = require('fs')
var { to } = require('../utility/func')
var { init } = require('../postgre/init/init')
var { report } = require('../utility/report')

var err, data
module.exports.main = (app, db) => {

  const upload = app.get('upload')

  app.post('/upload', upload.array('files'), (req, res) => {
    console.log('upload')
    return res.json({
      result: 'succeed',
      filenames: req.files.map(file=>file.filename)
    })
  })

  app.post('/submit', async (req, res) => {
    [err, data] = await report(db, req.body)
    res.json({ result: err? err: data })
  })

  app.post('/delete/:id', async (req, res) => {
    db.postgre = await db.Postgre.pool('postgres')
    const query = 'delete from \"Report\" where \"id\"=' + req.params.id;
    [err, data] = await to(db.postgre.query(query))
    res.json({ result: err? err: data })
  })

  app.post('/report', async (req, res) => {
    db.postgre = await db.Postgre.pool('postgres');
    [err, data] = await to(db.postgre.query('select * from \"Report\"'))
    res.json({ result: err? err: data.rows })
  })

  app.post('/init', async (req, res) => {
    [err, data] = await init(db)
    res.json({ result: err? err: data })
  })

}
