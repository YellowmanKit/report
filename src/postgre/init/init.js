var { to } = require('../../utility/func')
var { sql } = require('../sql')
var { Report } = require('./table/Report')

var err, data
module.exports.init = async db => {
  db.postgre = await db.Postgre.pool('postgres');
  [err, data] = await to(db.postgre.query('drop schema report cascade;'));
  [err, data] = await to(db.postgre.query('create schema report;'));
  [err, data] = await to(db.postgre.query('SET search_path TO \'report\';'));
  [err, data] = await create(db, [ Report ]);
  return [err, data]
}

const create = async (db, tables) => {
  for(var table of tables){
    [err, data] = await to(db.postgre.query(sql.create(table.name, table.fields)));
    [err, data] = await to(db.postgre.query(table.values.map(value => sql.insert(table.name ,value)).join(';')));
  }
  return [err, data]
}
