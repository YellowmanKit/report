module.exports.Report = {
  name: 'Report',
  fields: {
    id: 'serial primary key',
    timestamp: 'varchar(50)',
    mode: 'varchar(50)',
    date: 'varchar(50)',
    time: 'varchar(50)',
    address: 'varchar(500)',
    latitude: 'varchar(50)',
    longitude: 'varchar(50)',
    isRoadSign: 'boolean',
    isAccident: 'boolean',
    description: 'varchar(500)',
    photos: 'text[]'
  },
  values: [
  ]
}
