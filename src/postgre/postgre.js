const Pool = require('pg').Pool

const Postgre = {
  pool: (db_name, dev_mode) => Postgre.create(process.env, db_name, dev_mode),
  create: async ({
    PG_HOST,
    PG_USER,
    PG_PASSWORD,
    PG_HOST_DEV,
    PG_USER_DEV,
    PG_PASSWORD_DEV,
    PG_DATABASE_DEV,
    PG_PORT,
    DEV
  }, db_name, dev_mode) => {
    const dev = dev_mode !== undefined? dev_mode: DEV === 'TRUE'
    console.log('DB: ' + (dev? PG_DATABASE_DEV: db_name))
    const new_pool = new Pool({
      user: dev? PG_USER_DEV: PG_USER,
      host: dev? PG_HOST_DEV: PG_HOST,
      database: dev? PG_DATABASE_DEV: db_name,
      password: dev? PG_PASSWORD_DEV: PG_PASSWORD,
      port: PG_PORT
    })
    await new_pool.query('SET search_path TO \'report\';')
    return new_pool
  }
}
module.exports.Postgre = Postgre
