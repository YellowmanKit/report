const quote = value => '\"' + value + '\"'
const s_quote = value => '\'' + value + '\''

module.exports.sql = {
  create: (table, fields) => {
    var query = 'CREATE TABLE ' + quote(table) + '('
    for(var key in fields){
      query += quote(key) + ' ' + fields[key] + ', '
    }
    query = query.substring(0,query.length - 2) + ')'
    return query
  },
  alter: (table, values) => {
    var query = 'ALTER TABLE ' + quote(table)
    for(var i=0;i<values.length;i++){
      query += ' ADD COLUMN ' + values[i]
      if(i < values.length - 1){ query += ',' }
    }
    return query
  },
  select: (table, conditions, select) => {
    var query = 'SELECT ' + (select? '\"' + select + '\"':'*') + ' FROM ' + quote(table)
    if(conditions){ query += sql.conditions(conditions) }
    return query
  },
  insert: (table, data) => {
    var query = 'INSERT INTO ' + quote(table) + ' ('
    for(var key in data){
      query += quote(key) + ', '
    }
    query = query.substring(0, query.length - 2)
    query += ') VALUES ('
    for(var key in data){
      if(typeof data[key] === 'string'){
        if(data[key].includes('[')){
          query += '\'{' + data[key].substring(1).slice(0,-1) + '}\', '
        }else{
          query += s_quote(data[key]) + ', '
        }
      }
      else{ query += data[key] + ', ' }
    }
    query = query.substring(0, query.length - 2) + ')'
    return query
  },
  update: (table, values, conditions) => {
    var query = 'UPDATE ' + table + ' SET '
    for(var i=0;i<values.length;i++){
      query += values[i]
      if(i < values.length - 1){ query += ', ' }
    }
    query += sql.conditions(conditions)
    return query
  },
  conditions: (conditions) => {
    var query = ' WHERE '
    for(var key in conditions){
      if(typeof conditions[key] == 'string'){
        conditions[key] = s_quote(conditions[key])
      }
      query += quote(key) + '=' + conditions[key]
      query += ' AND '
    }
    query = query.substring(0,query.length - 5)
    return query
  }
}
