require('dotenv').config()
var express = require('express')
const app = express()
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const path = require('path')
const multer = require('multer')
const storagePath = path.join(__dirname, '../../data/storage/');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, storagePath)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})
var upload = multer({ storage: storage })
app.set('upload',upload)

var { Postgre } = require('./postgre/postgre')

var { main } = require('./router/main')
main(app, { Postgre })

var http = require('http')
app.server = http.createServer(app).listen(1026,
  ()=>{ console.log('report is running at port 1026') } )
